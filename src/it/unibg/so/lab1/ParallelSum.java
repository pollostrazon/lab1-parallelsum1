package it.unibg.so.lab1;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Scanner;

public class ParallelSum {
	
	private static int VALUES = 50;
	private static int THREADS = 10;
	
	private Integer[] values = new Integer[VALUES];	
	private SumThread[] threads = new SumThread[THREADS];
	
	public void readValues(Reader r) {
		Scanner in = new Scanner(r);
		int i=0;
		while(in.hasNext())
			values[i++] = Integer.valueOf(in.next());
		in.close();
	}
	
	public int sum() throws InterruptedException{
		threads = initParallelSum();
		
		for(Thread t: threads)
			t.start();
		
		int result = 0;
		for(SumThread t: threads){
			t.join();
			result += t.result;
		}
		
		return result;
	}
	
	private SumThread[] initParallelSum(){
		for(int i=0; i < THREADS; i++)
			threads[i] = new SumThread(subList(values, i));
		return threads;
	}
	
	private Integer[] subList(Integer[] list, int threadNumber){
		int subListSize = (int)VALUES/THREADS;
		return Arrays.copyOfRange(list, threadNumber*subListSize, (threadNumber*subListSize)+subListSize);
	}
	
	
	
	public static void main(String[] args) {
		ParallelSum parallelSum = new ParallelSum();
		parallelSum.readValues(new InputStreamReader(System.in));
		
		try {
			System.out.println(parallelSum.sum());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
